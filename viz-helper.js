const botjs = require("./bot");
const pdb = require("./propsdb");
const udb = require("./usersdb");
var viz = require('viz-js-lib');
viz.config.set('websocket','wss://vizlite.lexai.host/');
viz.config.set('address_prefix','VIZ');
viz.config.set('chain_id','2040effda178d4fffff5eab7a915d4019879f5205cc5392e4bcced2b6edda0cd');

async function propsList() {
try {
    const result = await viz.api.getChainPropertiesAsync();

const get_db_props = await pdb.getProps();
var change_count = 0;
for (a in result) {
    if (get_db_props[a] != result[a]) {
        change_count += 1;
        const user_info = await udb.findAllUsers();
        if (user_info) {
    for (let user in user_info) {
        if (user_info[user].subscribe && user_info[user].subscribe.length != 0 && !user_info[user].subscribe.includes(a)) continue;
        if (a !== 'flag_energy_additional_cost' && a !== 'max_curation_percent' && a !== 'min_curation_percent') {
            if (a === 'bandwidth_reserve_percent' || a === 'committee_request_approve_min_percent' || a === 'inflation_witness_percent' || a === 'inflation_ratio_committee_vs_reward_fund') {
await botjs.changePropsMSG(user_info[user], a, result[a]/100, 'percent');
            } else {
                await botjs.changePropsMSG(user_info[user], a, result[a], 'chislo');
            }
                    }
                }
    }
    }
}
if (change_count > 0) {
await pdb.updateProps(result);
}
} catch(e) {
console.log(e);
}
}

module.exports.propsList = propsList;