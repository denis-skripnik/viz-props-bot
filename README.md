# viz-props-bot
## Bot for VIZ, notifies about the change of parameters get_chain_properties.

Russion version: [README_RU.md](README_RU.md).

### Commands:
    1. help - help on commands;
    2. ub and unsub - subscription or unsubscribe from the parameters.  They a listed separated by commas.  Example: sub min_delegation, bandwidth_reserve_percent
unsub flag_energy_additional_cost.
Cancellation is valid only if you subscribe to someone.
    3. prop all - a list of parameters, as in the blockchain, prop min_delegation - the value of a specific parameter (in the example min_delegation, but you can specify any other)
    4. props - list of parameters in the plain language.
    5. Support - write to the Creator of the bot.

### In addition to teams
Sends notifications if parameters in get_chain_properties change.

## The Installation:
    1. Upload to the server;
    2. Go to a bot folder and enter
npm install;
    3. After the modules are installed, go to the bot.js file and change the values ​​to your api key and Telegram id:
``var token = '7154329:aaa-585';``
``var admin_id = 123456789;``

## The author of the bot is a the blind programmer and the delegate Denis Skripnik.
Account: https://viz.world/@denis-skripnik
Vote for witness: https://viz.world/witnesses/denis-skripnik.