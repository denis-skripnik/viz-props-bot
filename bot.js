const TeleBot = require('telebot');
var token = '7154329:aaa-585';
const bot = new TeleBot(token);
bot.start();
var admin_id = 123456789;
const udb = require("./usersdb");
const sdb = require("./supportdb");
const pdb = require("./propsdb");
const view = require("./viewer");
var viz = require('viz-js-lib');

Array.prototype.remove = function(value) {
    var idx = this.indexOf(value);
    if (idx != -1) {
        // Второй параметр - число элементов, которые необходимо удалить
        return this.splice(idx, 1);
    }
    return false;
}

async function ru_keybord(variant) {
    let replyMarkup;
    if (variant === 'standart') {
    replyMarkup = bot.keyboard([
        ["?", "параметры", "prop all", "Подписки", "Поддержка"],
], {resize: true});
} else if (variant === 'admin') {
replyMarkup = bot.keyboard([
        ["Да"],
], {resize: true});
    } else if (variant === 'lang') {
        replyMarkup = bot.keyboard([
            ["Eng", "Ru"],
    ], {resize: true});
}
var buttons = {
    replyMarkup};
    return buttons;
}

async function eng_keybord(variant) {
    let replyMarkup;
    if (variant === 'standart') {
replyMarkup = bot.keyboard([
                    ["?", "properties", "prop all", "subscribes", "support"],
        ], {resize: true});
    } else if (variant === 'admin') {
        replyMarkup = bot.keyboard([
            ["yes"],
], {resize: true});
        } else if (variant === 'lang') {
            replyMarkup = bot.keyboard([
                ["Eng", "Ru"],
    ], {resize: true});
    }
    var buttons = {
        replyMarkup};
    return buttons;
    }

    async function sendMSG(userId, text, type, lang) {
        try {
        if (lang === 'Ru') {
let keybord = await ru_keybord(type);
await bot.sendMessage(userId, text, keybord);
} else if (lang === 'Eng') {
            let keybord = await eng_keybord(type);
            await bot.sendMessage(userId, text, keybord);
} else {
    let keybord = await eng_keybord(type);
    await bot.sendMessage(userId, text, keybord);
}
} catch(e) {
console.log(e);
    if (e.error_code === 403) {
await udb.removeUser(userId);
}
}
}

async function startCommand() {
bot.on(/start|старт/, async function (msg, match) {
    var userId = msg.from.id;
    var username = msg.from.username;

const uid = {uid: userId, username: username, state:0, lang: ""};
const user = await udb.getUser(userId);
if (!user) {
await udb.addUser(uid);
    }
                      
let text = `Hello @${username}! It's a bot that notifies on change of parameters in the get_chain_properties.
Please select a language by clicking on one of the buttons below.

[/properties](/properties) - list of parameters (get_chain_properties). Are described understandable words.
If you want to see them as in the blockchain, use the [prop all](prop all) command;
prop parameter - displays the value of the specific parameter entered.
sub parameter - notification Subscription option;
unsub Parameter - unsubscribe from parameter notifications.

Привет, @${username}! Это бот, уведомляющий об изменении параметров get_chain_properties.
Пожалуйста, выберите язык, нажав на одну из кнопок ниже.

[/properties](/properties) - Список параметров (get_chain_properties). Описаны понятными словами.
Если хотите увидеть их, как в блокчейне, воспользуйтесь командой /[prop all](prop all);
prop параметр - выводит значение конкретного введённого параметра.
sub параметр - Подписка на уведомление о параметре;
unsub Параметр - отписка от уведомлений о параметре.`;
                        await sendMSG(userId, text, 'lang', 'eng');
});
}

async function changePropsMSG(user, prop, value, type) {
if (type === 'percent') {
value = value + '%';
}

if (user.lang === 'Ru') {
let text = `Значение параметра ${prop} изменилось. Сейчас: ${value}.

/help - Справка по командам бота.`;
                        await sendMSG(user.uid, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
    let text = ` The parameter value ${prop} has changed. Now: ${value}.
    /help - List of commands.`;
    await sendMSG(user.uid, text, 'standart', 'Eng');
}
}

async function propCommand() {
    bot.on(/prop (.+)/i, async function (msg, pr) {
    var fromId = msg.from.id;
    var parametr = pr.match[1].toLowerCase();
    try {
const get_db_props = await pdb.getProps();
var viz_props = [];
for (let prop in get_db_props) {
if (prop !== '_id') {
    viz_props.push({[prop]: get_db_props[prop]});
}
}
const user = await udb.getUser(fromId);
if (parametr !== 'all') {
if (user.lang === 'Ru') {
    let text = `${parametr}: ${get_db_props[parametr]}
/help - Справка по командам бота.`;
await sendMSG(fromId, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
let text = `${parametr}: ${viz_props[parametr]}
/help - List of commands.`;
await sendMSG(fromId, text, 'standart', 'Eng');
}
} else {
if (user.lang === 'Ru') {
    let text = `${JSON.stringify(viz_props)}
/help - Справка по командам бота.`;
    await sendMSG(fromId, text, 'standart', 'Ru');
    } else if (user.lang === 'Eng') {
        let text = `${JSON.stringify(get_db_props)}
/help - List of commands.`;
    await sendMSG(fromId, text, 'standart', 'Eng');
    }
    }
} catch(e) {
console.log(e);
}
});
}

async function propertiesCommand() {
    bot.on(/properties|параметры/i, async function (msg, match) {
    var userId = msg.from.id;
const user = await udb.getUser(userId);
if (user) {
var props_array = [];
const get_db_props = await pdb.getProps();
if (user.lang === 'Ru') {
props_array.length = 0;
props_array.push({'Передаваемая комиссия при создании аккаунта': get_db_props.account_creation_fee, 'Максимальный размер блока в сети (байт)': get_db_props.maximum_block_size, 'Коэффициент делегирования при создании аккаунта': get_db_props.create_account_delegation_ratio, 'Время делегирования при создании аккаунта (секунд)': get_db_props.create_account_delegation_time, 'Минимальное количество токенов при делегировании': get_db_props.min_delegation, 'Доля инфляции для награды делегатам (процент)': get_db_props.inflation_witness_percent/100, 'Соотношение разделения остатка инфляции между комитетом и фондом наград (процент)': get_db_props.inflation_ratio_committee_vs_reward_fund/100, 'Количество блоков между пересчетом инфляционной модели': get_db_props.inflation_recalc_period, 'Доля сети, выделяемая для резервной пропускной способности (процент)': get_db_props.bandwidth_reserve_percent/100, 'Резервная пропускная способность действует для аккаунтов с долей сети до порога': get_db_props.bandwidth_reserve_below, 'Минимальный вес голоса для учета при голосовании за контент (rshares)': get_db_props.vote_accounting_min_rshares, 'Минимальный процент доли сети голосующих необходимый для принятия решения по заявке в комитете': get_db_props.committee_request_approve_min_percent/100});
        var props_view = JSON.stringify(props_array, null, ' ');
let text = `${props_view}
/help - Справка по командам бота.`;
await sendMSG(userId, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
props_array.length = 0;
props_array.push({'Transmitted by the Commission when creating your account': get_db_props.account_creation_fee, 'Maximum block size on the network (bytes)': get_db_props.maximum_block_size, 'Delegation ratio when creating an account': get_db_props.create_account_delegation_ratio, 'The time of the delegation when during the account creation process (the seconds)': get_db_props.create_account_delegation_time, 'Minimum number of the tokens when delegating': get_db_props.min_delegation, 'Percentage of inflation for award delegates (percentage)': get_db_props.inflation_witness_percent/100, 'The ratio of division of the balance of inflation between the Committee and Foundation awards (percent)': get_db_props.inflation_ratio_committee_vs_reward_fund/100, 'Number of blocks between the recalculation of the inflation model': get_db_props.inflation_recalc_period, 'The proportion of the network that is allocated to reserve capacity (percentage)': get_db_props.bandwidth_reserve_percent/100, 'The reserve bandwidth is valid for accounts with a network share up to the threshold': get_db_props.bandwidth_reserve_below, 'The minimum the voice weight to take into account when voting for the content (rshares)': get_db_props.vote_accounting_min_rshares, 'The minimum percentage of the voting network required to make a decision on an application to the Committee': get_db_props.committee_request_approve_min_percent/100});
        var props_view = JSON.stringify(props_array, null, ' ');
let text = `${props_view}
/help - List of commands.`;
await sendMSG(userId, text, 'standart', 'Eng');
}

}
});
}

async function helpCommand() {
    bot.on(/help|\?/i, async function (msg, match) {
        var fromId = msg.from.id;
        
    const user = await udb.getUser(fromId);
    if (user.lang === 'Ru') {
    let text = `[/properties](/properties) - Список параметров (get_chain_properties). Описаны понятными словами.
    Если хотите увидеть их, как в блокчейне, воспользуйтесь командой [prop all](prop all);
    prop параметр - выводит значение конкретного введённого параметра.
    sub параметр - Подписка на уведомление о параметре;
    unsub параметр - отписка от уведомлений о параметре.`;
                            await sendMSG(user.uid, text, 'standart', 'Ru');
    } else if (user.lang === 'Eng') {
    let text = `[/properties](/properties) - list of parameters (get_chain_properties). Are described understandable words.
    If you want to see them as in the blockchain, use the [prop all](prop all) command;
    prop parameter - displays the value of the specific parameter entered.
    sub parameter - notification Subscription option;
    unsub parameter - unsubscribe from parameter notifications.`;
    await sendMSG(user.uid, text, 'standart', 'Eng');
    }
});
}

async function subscribeCommand() {
            bot.on(/^sub (.+)/i, async function (msg, pr) {
                var fromId = msg.from.id;
                var username = msg.from.username;
                var sb = pr.match[1].toLowerCase();
                if (sb.indexOf(',') > -1) {
                    var sp = sb.split(',');
                } else {
                var sp = [sb];
                }
try {
    const user = await udb.getUser(fromId);
                if(user) {
            var user_subscribes = [];
            sp.forEach(async function (el) {
            if (user.subscribe && !user.subscribe.includes(el)) {
                        user_subscribes = user.subscribe;
                                user_subscribes.push(el);
                    } else if (!user.subscribe) {
                        user_subscribes.push(el);
                    } else if (user.subscribe && user.subscribe.includes(sub_user)) {
                        user_subscribes = user.subscribe;
                   }
                });

                var sp_str = sp.join(',');

                   const user_data = {uid: fromId, username: username, subscribe: user_subscribes, state:0, lang: user.lang};
                   const res = await udb.updateUser(fromId, user_data);
                   if (res) {
if (user.lang === 'Ru') {
                    let text = `Вы успешно подписались на получение уведомлений о параметрах ${sp_str}.
                    /help - Справка по командам бота.`;
await sendMSG(fromId, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
    let text = `you have successfully subscribed to properties reward notifications: ${sp_str}.
    /help - List of commands.`;
    await sendMSG(fromId, text, 'standart', 'Eng');
                }
            }
        }
    } catch(e) {
        console.log(e);
        }
    });
    }

            async function unsubscribeCommand() {
                bot.on(/^unsub (.+)/i, async function (msg, pr) {
                    var fromId = msg.from.id;
                    var username = msg.from.username;
                    var sb = pr.match[1].toLowerCase();
                    if (sb.indexOf(',') > -1) {
                        var sp = sb.split(',');
                    } else {
                    var sp = [sb];
                    }

                    const user = await udb.getUser(fromId);
                if(user) {
                    var user_subscribes = [];
                        sp.forEach(async function (el) {
                    if (user.subscribe && user.subscribe.includes(el)) {
                        user_subscribes = user.subscribe;
                                                user_subscribes.remove(el);
                    }
                });
                    
                    if (user_subscribes) {
                        const user_data = {uid: fromId, username: username, subscribe: user_subscribes, state:0, lang: user.lang};
                        const res = await udb.updateUser(fromId, user_data);
                        if (res) {
                            if (user.lang === 'Ru') {
                            let text = `Вы одписались от уведомлений о параметрах ${sb}.
                            /help - Список команд.`;
await sendMSG(fromId, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
                                let text = `you have unsubscribed from the ${sb} properties.
                                /help - List of commands.`;
                                await sendMSG(fromId, text, 'standart', 'Eng');
                            }
                }
                } else {
if (user.lang === 'Ru') {
                    let text = `Параметров ${sp} нет в списке ваших подписок.`;
await sendMSG(fromId, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
    let text = `${sp} properties are not in your subscription list.`;
    await sendMSG(fromId, text, 'standart', 'Eng');
}
                }
            }
                    });
                }

                async function subscribesCommand() {
                        bot.on(/subscribes|Подписки/i, async function (msg, match) {
                            var fromId = msg.from.id;
                            var username = msg.from.username;
                        
const user = await udb.getUser(fromId);
            if (user && user.subscribe) {
            var subscribes = '';
                user.subscribe.forEach(async function (subscribe_el) {
            subscribes += subscribe_el + ',';
            });
            subscribes = subscribes.replace(/,\s*$/, "");
            if (user.lang === 'Ru') {
let text = `Ваши подписки:

${subscribes}

/help - список команд.`;
await sendMSG(fromId, text, 'standart', 'Ru');
            } else if (user.lang === 'Eng') {
                let text = `Your subscriptions:

${subscribes}

/help - list of commands.`;
await sendMSG(fromId, text, 'standart', 'Eng');
            }
                                        } else {
if (user.lang === 'Ru') {
                                            let text = `Вы не подписывались.

/help - список команд.`;
await sendMSG(fromId, text, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
    let text = `you have not subscribed.

/help - list of commands.`;
await sendMSG(fromId, text, 'standart', 'Eng');
}
                                                                           }
            });
        }

        async function adminCommand() {
            bot.on(/admin ((.|\n)+)/, async function (msg, pr) {
            var fromId = msg.from.id;
            var username = msg.from.username;
            var resp = pr.match[1];
            if (fromId === admin_id) {
            const user_info = await udb.findAllUsers();
            if (user_info) {
            user_info.forEach(async function (user) {
            if (user.lang === 'Ru') {
                let text = resp + `

Если вы получили сообщение, просьба нажать на /yes или ввести
            Да
Также вы можете нажать на одноимённую кнопку.

Надо убедиться, что вы получили это сообщение.`;
            await sendMSG(user['uid'], text, 'admin', 'Ru');
            } else if (user.lang === 'Eng') {
                let text = resp + `

If you receive a message, please click /yes or enter
                             Yes
You can also click on the button with the same name.
                
                We need to make sure you received this message.`;
            await sendMSG(user['uid'], text, 'admin', 'Eng');
            }
        });
            }
                            }
                        });
                    }
            
        async function yesCommand() {
       bot.on(/yes|Да/i, async function (msg, match) {
var fromId = msg.from.id;
        var fromLogin = msg.from.username;
        var toId = admin_id;

let textTo = `Пользователь Telegram @${fromLogin} подтвердил получение вашего сообщения.`;
await sendMSG(toId, textTo, 'standart');
const user = await udb.getUser(fromId);
            if (user) {
if (user.lang === 'Ru') {
                let textFrom = `Благодарю за подтверждение. Оно отправлено успешно.`;
await sendMSG(fromId, textFrom, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
    let textFrom = `Thank you for your confirmation. It was sent successfully.`;
    await sendMSG(fromId, textFrom, 'standart', 'Eng');
}
}
});
    }

    async function supportCommand() {
        bot.on(/support|^Поддержка/i, async function (msg, match) {
            var fromId = msg.from.id;
            var username = msg.from.username;

const user = await udb.getUser(fromId);
                if(user) {
                   const user_data = {uid: fromId, username: username, subscribe: user.subscribe, state:1, lang: user.lang};

                       const res = await udb.updateUser(fromId, user_data);
                if (res) {
if (user.lang === 'Ru') {
                    let text = 'Введите пожалуйста сообщение создателю бота.';
                    await sendMSG(fromId, text, '', 'Ru');
} else if (user.lang === 'Eng') {
    let text = `please Enter a message to the Creator of the bot.`;
    await sendMSG(fromId, text, '', 'Eng');
}
                }
        }
    });
    }

    async function nullSupportCommand() {
                        bot.on('text', async (msg) => {
                var fromId = msg.from.id;
                var username = msg.from.username;
var toId = admin_id;
                
                const user = await udb.getUser(fromId);
switch(user.state) {
case 1:
const user_data = {uid: fromId, username: username, subscribe: user.subscribe, state:0, lang: user.lang};
const update_user = await udb.updateUser(fromId, user_data);
if (update_user) {
const timezoneOffset = (new Date()).getTimezoneOffset() * 60000;
const date = await view.date_str(msg.date*1000 - timezoneOffset, true, false, true);
await sdb.addSupportMSG(username, date, msg.text);
let textTo = `Пользователь @${username} оставил сообщение в #поддержка:

${msg.text}`;
await sendMSG(toId, textTo, 'standart');
if (user.lang === 'Ru') {
let textFrom = `Благодарю. Сообщение успешно отправлено.`;
await sendMSG(fromId, textFrom, 'standart', 'Ru');
} else if (user.lang === 'Eng') {
    let textFrom = 'Thank you. The message was successfully sent.';
    await sendMSG(fromId, textFrom, 'standart', 'Eng');
}
               }
    break;
}
        });
    }

    async function chatCommand() {
        bot.on(/чат/, async function (msg, match) {
            var fromId = msg.from.id;
            var username = msg.from.username;

            if (fromId === admin_id) {
const supports = await sdb.findAllSupportMSG();
let support_list = '';
supports.forEach(async function (msg) {
support_list += `Дата: ${msg.datetime}, Автор: @${msg.login}
Текст:
${msg.text}

`;
});
let text = `Список сообщений от пользователей бота:
                
${support_list}
`;
await sendMSG(fromId, text, 'standart', 'Ru');
}
        });
    }

        async function langRuCommand() {
            bot.on(/Ru/i, async function (msg, match) {
                var fromId = msg.from.id;
                var username = msg.from.username;
    
    const user = await udb.getUser(fromId);
                    if(user) {
                       const user_data = {uid: fromId, username: username, subscribe: user.subscribe, state:0, lang: "Ru"};
    
                           const res = await udb.updateUser(fromId, user_data);
                    if (res) {
    let text = `Выбран язык: Русский.

/help - Список команд.`;
    await sendMSG(fromId, text, 'standart', 'Ru');
                    }
            }
        });
        }
    
        async function langEngCommand() {
            bot.on(/Eng/i, async function (msg, match) {
                var fromId = msg.from.id;
                var username = msg.from.username;
    
    const user = await udb.getUser(fromId);
                    if(user) {
                       const user_data = {uid: fromId, username: username, subscribe: user.subscribe, state:0, lang: "Eng"};
    
                           const res = await udb.updateUser(fromId, user_data);
                    if (res) {
    let text = `Selected language: English.

/help - list of commands.`;
    await sendMSG(fromId, text, 'standart', 'Eng');
                    }
            }
        });
        }
    
async function langNotifyMSG() {
    const user_info = await udb.findAllUsers();
    if (user_info) {
for (let user in user_info) {
if (!user_info[user].lang || user_info[user].lang === "") {
    const user_data = {uid: user_info[user]['uid'], username: user_info[user]['username'], subscribe: user_info[user]['subscribe'], state:0, lang: "yes_no"};
    
    const res = await udb.updateUser(user_info[user]['uid'], user_data);
    let text = `Please select your language. The bot does not know in what language to send you notifications.
Выберите язык, пожалуйста. Бот пока не знает, на каком языке присылать вам уведомления.`;
await sendMSG(user_info[user]['uid'], text, 'lang', '');
}
}
    }
}

    module.exports.startCommand = startCommand;
    module.exports.changePropsMSG = changePropsMSG;
    module.exports.propCommand = propCommand;
    module.exports.propertiesCommand = propertiesCommand;
    module.exports.helpCommand = helpCommand;
    module.exports.adminCommand = adminCommand;
    module.exports.subscribeCommand = subscribeCommand;
    module.exports.unsubscribeCommand = unsubscribeCommand;
    module.exports.subscribesCommand = subscribesCommand;
    module.exports.yesCommand = yesCommand;
    module.exports.supportCommand = supportCommand;
    module.exports.nullSupportCommand = nullSupportCommand;
    module.exports.chatCommand = chatCommand;
    module.exports.langEngCommand = langEngCommand;
    module.exports.langRuCommand = langRuCommand;
    module.exports.langNotifyMSG = langNotifyMSG;