var Datastore = require('nedb')
  , db = new Datastore({ filename: 'props.db', autoload: true });
  db.persistence.setAutocompactionInterval(1000 * 300);

async function updateProps(result) {
  db.update({}, result, {upsert:true}, () => {});
}

function getProps() {
  return new Promise((resolve, reject) => {
      db.findOne({}, (err,data) => {
             if(err) {
                    reject(err);
             } else {
                    resolve(data);
             }
      });
  });
}

module.exports.updateProps = updateProps;
module.exports.getProps = getProps;