const botjs = require("./bot");
const vizh = require("./viz-helper");
        
            
async function timer() {
await vizh.propsList();
    await botjs.langNotifyMSG();
}

setInterval(timer, 1800000);

async function run() {
await botjs.startCommand();
await botjs.propCommand();
await botjs.propertiesCommand();
await botjs.helpCommand();
await botjs.adminCommand();
await botjs.subscribeCommand();
await botjs.unsubscribeCommand();
await botjs.subscribesCommand();
await botjs.yesCommand();
await botjs.supportCommand();
await botjs.nullSupportCommand();
await botjs.chatCommand();
await botjs.langEngCommand();
await botjs.langRuCommand();
}

run();